parser grammar VSLParser;

options {
  language = Java;
  tokenVocab = VSLLexer;
}

@header {
  package TP2;

  import java.util.stream.Collectors;
  import java.util.Arrays;
}


// TODO : other rules

program returns [TP2.ASD.Program out]
    : e=expression EOF { $out = new TP2.ASD.Program($e.out); } // TODO : change when you extend the language
   // | a = affectation EOF { $out = new TP2.ASD.Program($a.out);}
    ;

expression returns [TP2.ASD.Expression out]
    : l=factor PLUS r=expression  { $out = new TP2.ASD.AddExpression($l.out, $r.out); }
    | l=factor MINUS r=expression  { $out = new TP2.ASD.SubExpression($l.out, $r.out); }
    | l=factor TIMES r=expression  { $out = new TP2.ASD.TimesExpression($l.out, $r.out);}
    | l=factor DIV r=expression  { $out = new TP2.ASD.DivExpression($l.out, $r.out);}
    | f=factor { $out = $f.out; }
    
    // TODO : that's all?
    ;
/*
affectation returns [TP2.ASD.Affectation out]
	:l=ident AFFECT r=expression {$out = new TP2.ASD.Affectation($l.out, $r.out);}
*/
factor returns [TP2.ASD.Expression out]
    : p=primary { $out = $p.out; }
    // TODO : that's all?
    ;

primary returns [TP2.ASD.Expression out]
    : INTEGER { $out = new TP2.ASD.IntegerExpression($INTEGER.int); }
    // TODO : that's all?
    ;
/*    
ident returns [TP2.ASD.IntAddress out]
	: IDENT   { $out = new TP2.ASD.IdentExpression($IDENT.string); }
	;    
*/
