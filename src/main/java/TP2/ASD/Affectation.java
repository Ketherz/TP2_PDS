package TP2.ASD;

import TP2.Llvm;
import TP2.TypeException;
import TP2.Utils;
import TP2.ASD.Expression.RetExpression;

public class Affectation {
    Expression left;
    Expression right;

    public Affectation(IdentExpression left, Expression right) {
      this.left = left;
      this.right = right;
    }

    // Pretty-printer
    public String pp() {
      return left.pp() + " = " + right.pp();
    }

    // IR generation
    /*
	static public class RetAffectation {
		public RetAffectation toIR() throws TypeException {
			RetExpression leftRet = left.toIR();
			RetExpression rightRet = right.toIR();

			// We base our build on the left generated IR:
			// append right code
			leftRet.ir.append(rightRet.ir);

			// new add instruction result = left + right
			Llvm.Instruction store = new Llvm.Store(leftRet.type.toLlvmType(), leftRet.result, rightRet.result,
					rightRet.type.toLlvmType());

			// append this instruction
			leftRet.ir.appendCode(store);

			// return the generated IR, plus the type of this expression
			// and where to find its result
			return new RetAffectation(leftRet.ir, leftRet.type, rightRet.type);
		}
	}
	*/
   
}
