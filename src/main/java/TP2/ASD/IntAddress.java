package TP2.ASD;

import TP2.Llvm;

public class IntAddress extends Type {
    public String pp() {
      return "INT*";
    }

    @Override public boolean equals(Object obj) {
      return obj instanceof IntAddress;
    }

    public Llvm.Type toLlvmType() {
      return new Llvm.IntAddress();
    }
  }
