package TP2.ASD;

import TP2.Llvm;
import TP2.ASD.Expression.RetExpression;

public class IdentExpression extends Expression {
	String value;

	public IdentExpression(String value) {
		this.value = value;
	}

	public String pp() {
		return "" + value;
	}

	public RetExpression toIR() {
		// Here we simply return an empty IR
		// the `result' of this expression is the integer itself (as string)
		return new RetExpression(new Llvm.IR(Llvm.empty(), Llvm.empty()), new IntAddress(), "%" + value);
	}
}

